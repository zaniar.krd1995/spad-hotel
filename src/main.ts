import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

const microserviceOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'localhost:50053',
    package: 'hotel',
    protoPath: join(__dirname, '../src/hotel.proto'),
  },
}

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, microserviceOptions);
  await app.listen(); 
}
bootstrap();

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HotelDocument = Hotel & Document;

@Schema()
export class Hotel {
  @Prop({type: String, required: true})
  name: string;

  @Prop({type: Number, required: true})
  stars: number;
}

export const HotelSchema = SchemaFactory.createForClass(Hotel);
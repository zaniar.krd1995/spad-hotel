import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { HotelsService } from './hotels.service';
import { CreateHotelDTO } from './interface/create-hotel.interface';
import { HotelResponseDTO } from './interface/hotel-response.interface';

@Controller('hotels')
export class HotelsController {

  constructor(private readonly service: HotelsService) {}

  @GrpcMethod('HotelService', 'CreateHotel')
  async createHotel(dto: CreateHotelDTO, metadata: any): Promise<HotelResponseDTO> {    
    throw new Error('Not implemented');
  }


}
